package com.weather_data.weather_api.Data;


import com.weather_data.weather_api.app.Weather;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.time.LocalDate;
import java.util.Iterator;

import java.net.HttpURLConnection;
import java.net.URL;


@Component
public class Periodic_Task {

    @Autowired
    private final ApplicationEventPublisher applicationEventPublisher;

    public Periodic_Task(ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
    }

    @Scheduled(fixedRate = 10000000, initialDelay = 500)
    public void FetchData() {
        System.out.println("new update fetch time");

        String url ="https://wttr.in/Koblenz?format=j1";

        applicationEventPublisher.publishEvent(new WeatherUpdate(this,FetchJsonData(url)));

    }

    private Weather FetchJsonData(String url){

        int currtempC = 0,maxtempC = 0,mintempC = 0,avergetempC = 0;

        try {
                URL obj = new URL(url);
                HttpURLConnection con = (HttpURLConnection) obj.openConnection();

                BufferedReader in = new BufferedReader(
                        new InputStreamReader(con.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
            //GetAllkeys(myResponse);
                JSONObject myResponse = new JSONObject(response.toString());

                JSONArray array = myResponse.getJSONArray("weather");

                currtempC =  Integer.parseInt(myResponse.getJSONArray("current_condition").getJSONObject(0).getString(Temperature.temp_C.toString())) ;

                maxtempC  =  Integer.parseInt(array.getJSONObject(0).getString(Temperature.maxtempC.toString()));

                mintempC  =  Integer.parseInt(array.getJSONObject(0).getString(Temperature.mintempC.toString()));

                avergetempC = Integer.parseInt(array.getJSONObject(0).getString(Temperature.avgtempC.toString()));


        }
        catch (MalformedURLException e)
        {
            e.printStackTrace();
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }


        Weather weather = new Weather(currtempC,maxtempC,mintempC,avergetempC, LocalDate.now());
        return weather;
    }

    private void GetAllkeys(JSONObject jsonObject){
        try {

            Iterator<String> keys = jsonObject.keys();
            while(keys.hasNext()) {
                String key = keys.next();
                //System.out.println(key);
                if(jsonObject.get(key) instanceof JSONArray) {
                    JSONArray array = (JSONArray) jsonObject.get(key);
                    JSONObject object = (JSONObject) array.get(0);
                    Iterator<String> innerKeys = object.keys();
                    while(innerKeys.hasNext()) {
                        String innerKey = innerKeys.next();
                        //System.out.println(innerKey);
                    }
                }
            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
