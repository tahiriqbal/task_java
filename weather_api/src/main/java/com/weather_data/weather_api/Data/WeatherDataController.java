package com.weather_data.weather_api.Data;

import com.weather_data.weather_api.app.WeatherAppController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class WeatherDataController implements ApplicationListener<WeatherUpdate> {

    @Autowired
    private final WeatherAppController weatherAppController;

    public WeatherDataController(WeatherAppController weatherAppController) {
        this.weatherAppController = weatherAppController;
    }

    @Override
    public void onApplicationEvent(WeatherUpdate weatherUpdate) {

        weatherAppController.SetWeatherUpdate(weatherUpdate.getWeather());
    }
}
