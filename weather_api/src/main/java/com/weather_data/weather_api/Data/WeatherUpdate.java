package com.weather_data.weather_api.Data;


import com.weather_data.weather_api.app.Weather;
import org.springframework.context.ApplicationEvent;

public class WeatherUpdate extends ApplicationEvent {

    private Weather weather;

    public WeatherUpdate(Object source, Weather weather) {
        super(source);
        this.weather=weather;
    }

    public Weather getWeather() {
        return weather;
    }
}
