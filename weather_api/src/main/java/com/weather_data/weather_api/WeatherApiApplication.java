package com.weather_data.weather_api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;


@EnableScheduling
@SpringBootApplication
public class WeatherApiApplication
{

	public static void main(String[] args){
		SpringApplication.run(WeatherApiApplication.class, args);
	}
}
