package com.weather_data.weather_api.app;

import javax.persistence.*;
import java.time.LocalDate;

@Table(name = "Weather")
@Entity
public class Weather {

    @Id
    @SequenceGenerator(
            name = "weather_sequence",
            sequenceName="weather_sequence",
            allocationSize=1
    )
    @GeneratedValue(
            strategy  = GenerationType.SEQUENCE,
            generator = "weather_sequence"
    )
    private Long id;
    private int currentTemp;
    private int maxTemp;
    private int minTemp;
    private int averageTemp;
    private LocalDate date;

    public Weather(Long id, int currentTemp, int maxTemp, int minTemp, int averageTemp, LocalDate date) {
        this.id = id;
        this.currentTemp = currentTemp;
        this.maxTemp = maxTemp;
        this.minTemp = minTemp;
        this.averageTemp = averageTemp;
        this.date = date;
    }

    public Weather(int currentTemp, int maxTemp, int minTemp, int averageTemp, LocalDate date) {
        this.currentTemp = currentTemp;
        this.maxTemp = maxTemp;
        this.minTemp = minTemp;
        this.averageTemp = averageTemp;
        this.date = date;
    }

    public Weather() {

    }

    public Weather(int maxTemp, int minTemp, int averageTemp, LocalDate date) {
        this.maxTemp = maxTemp;
        this.minTemp = minTemp;
        this.averageTemp = averageTemp;
        this.date = date;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getCurrentTemp() {
        return currentTemp;
    }

    public void setCurrentTemp(int currentTemp) {
        this.currentTemp = currentTemp;
    }

    public int getMaxTemp() {
        return maxTemp;
    }

    public void setMaxTemp(int maxTemp) {
        this.maxTemp = maxTemp;
    }

    public int getMinTemp() {
        return minTemp;
    }

    public void setMinTemp(int minTemp) {
        this.minTemp = minTemp;
    }

    public int getAverageTemp() {
        return averageTemp;
    }

    public void setAverageTemp(int averageTemp) {
        this.averageTemp = averageTemp;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }


    @Override
    public String toString() {
        return "Weather{" +
                "id=" + id +
                ", currentTemp=" + currentTemp +
                ", maxTemp=" + maxTemp +
                ", minTemp=" + minTemp +
                ", averageTemp=" + averageTemp +
                ", date=" + date +
                '}';
    }
}
