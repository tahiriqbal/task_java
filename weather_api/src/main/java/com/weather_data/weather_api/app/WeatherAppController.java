package com.weather_data.weather_api.app;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "v1/weather")
public class WeatherAppController {

    @Autowired
    private final WeatherAppService weatherAppService;

    public WeatherAppController(WeatherAppService weatherAppService) {
        this.weatherAppService = weatherAppService;
    }

    public  void SetWeatherUpdate(Weather weather){
        weatherAppService.SetWeatherUpdate(weather);
    }

    @GetMapping(path = "GetWeather")
    public Weather getWeather(){
        return weatherAppService.getWeather();
    }

}
