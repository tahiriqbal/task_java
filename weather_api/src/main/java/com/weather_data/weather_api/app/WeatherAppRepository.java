package com.weather_data.weather_api.app;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;


import java.util.List;

@Repository
public interface WeatherAppRepository extends JpaRepository<Weather,Long> {

    List<Weather> findWeatherByDate(LocalDate date);
    @Query(value = "SELECT max (currentTemp) FROM Weather ")
    public Integer max();
    @Query(value = "SELECT min (currentTemp) FROM Weather")
    public Integer min();
}
