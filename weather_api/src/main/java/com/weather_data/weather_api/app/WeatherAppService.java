package com.weather_data.weather_api.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.*;
import java.util.logging.Logger;

@Service
public class WeatherAppService {

    static Logger log = Logger.getLogger(String.valueOf(WeatherAppService.class));
    @Autowired
    private final WeatherAppRepository weatherAppRepository;

    public WeatherAppService(WeatherAppRepository weatherAppRepository) {
        this.weatherAppRepository = weatherAppRepository;
    }

    public void SetWeatherUpdate(Weather weather)
    {
        weatherAppRepository.save(weather);
    }

    public double GetWeather_(){
        List<Weather> weatherArrayList= weatherAppRepository.findWeatherByDate(LocalDate.now());
        double average = weatherArrayList.stream().mapToDouble(Weather::getCurrentTemp).average().orElse(0.0);
        System.out.println(average);
        return  average;
    }

    public Weather getWeather() {
        GetWeather_();

        List<Weather> weatherList=weatherAppRepository.findWeatherByDate(LocalDate.now());

        Weather weathermax=Collections.max(weatherList, new Comparator<Weather>() {
            @Override
            public int compare(Weather o1, Weather o2) {
                return Integer.compare(o1.getMaxTemp(),o2.getMaxTemp());
            }
        });

        Weather weathermin=Collections.min(weatherList, new Comparator<Weather>() {
            @Override
            public int compare(Weather o1, Weather o2) {
                return Integer.compare(o1.getMinTemp(), o2.getMinTemp());
            }
        });
        int weatherAverage = (int)weatherList.stream().mapToDouble(Weather::getCurrentTemp).average().orElse(0.0);

        Weather weatherResult = new Weather(0l,weatherList.get(weatherList.size()-1).getCurrentTemp(),weathermax.getMaxTemp(),weathermin.getMinTemp(),weatherAverage,LocalDate.now());
        return weatherResult;
    }
}
