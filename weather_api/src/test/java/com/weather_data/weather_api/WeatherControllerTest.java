package com.weather_data.weather_api;


import com.fasterxml.jackson.databind.ObjectMapper;

import com.weather_data.weather_api.Data.Periodic_Task;
import com.weather_data.weather_api.app.Weather;
import com.weather_data.weather_api.app.WeatherAppRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import javax.transaction.Transactional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@ActiveProfiles(value="test")
@SpringBootTest
@EnableWebMvc
public class WeatherControllerTest {

    @Autowired
    WebApplicationContext wac;
    @Autowired
    private ObjectMapper objectMapper;
    private MockMvc mockMvc;
    @Autowired
    private WeatherAppRepository userRepository;
    @Autowired
    private Periodic_Task periodic_task;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders
                .webAppContextSetup(wac)
                .build();
    }

    private void setupUser(){
        Weather weather = new Weather();
        weather.setAverageTemp(66);
        weather.setCurrentTemp(1);
        weather.setMaxTemp(68);
        weather.setMinTemp(-60);
        userRepository.save(weather);
    }


    @Test
    public void getAllUserWithBasicInfo() throws Exception {
        setupUser();
        mockMvc.perform(get("/v1/weather/GetWeather"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                //  .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Applicant Test"))
                .andDo(print());

    }


}
